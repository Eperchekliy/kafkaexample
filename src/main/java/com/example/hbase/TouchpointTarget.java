package com.example.hbase;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.Set;

@XmlRootElement
@XmlSeeAlso(value = {
        TouchpointBannerTarget.class,
        TouchpointSiteIdTarget.class,
        TouchpointNetworkTarget.class,
        TouchpointListTarget.class,
        TouchpointChainTarget.class
})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME, include = com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY, property = "type")
@com.fasterxml.jackson.annotation.JsonAutoDetect(creatorVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        getterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE,
        setterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE,
        fieldVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        isGetterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE)
@com.fasterxml.jackson.annotation.JsonSubTypes({
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = TouchpointBannerTarget.class, name = "banner"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = TouchpointSiteIdTarget.class, name = "site_id"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = TouchpointNetworkTarget.class, name = "network"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = TouchpointListTarget.class, name = "list"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = TouchpointChainTarget.class, name = "chain")
})
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonTypeInfo(use = org.codehaus.jackson.annotate.JsonTypeInfo.Id.NAME, include = org.codehaus.jackson.annotate.JsonTypeInfo.As.PROPERTY, property = "type")
@org.codehaus.jackson.annotate.JsonAutoDetect(creatorVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        getterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE,
        setterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE,
        fieldVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        isGetterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE)
@org.codehaus.jackson.annotate.JsonSubTypes({
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = TouchpointBannerTarget.class, name = "banner"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = TouchpointSiteIdTarget.class, name = "site_id"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = TouchpointNetworkTarget.class, name = "network"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = TouchpointListTarget.class, name = "list"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = TouchpointChainTarget.class, name = "chain")
})
public abstract class TouchpointTarget implements TargetCondition {

    /**
     * {@inheritDoc}
     */
    public boolean eval(AdvertisementContext context) {
        return this.eval(context.getTouchpoint());
    }

    /**
     * Evalute the specified touchpoint context to determine if some attribute of the touchpoint is
     * a match to the specified target.
     *
     * @param context THe context of the current touchpoint for evaluation
     * @return Returns true if the context is valid and matches the target.
     */
    public abstract boolean eval(TouchpointContext context);

    /**
     * Use the query builder to append our SOLR query that can be used
     * to find the specified touchpoint.
     *
     * @param qb The query builder to modify with the SOLR queries.
     */
    public abstract void buildQuery(QueryBuilder qb);

    /**
     * Interface for collecting multiple queries and appending them together to
     * create AND and OR conditions.
     *
     * @author Terrance A. Snyder
     *
     */
    public static interface QueryBuilder {

        /**
         * Returns a list provider that can be used when asking for the items that make
         * up a particular list of touchpoints. Used when targeting touchpoints via static
         * defined lists.
         *
         * @return Returns the list provider that can take a touchpoint list and extract it's value from nested queries (nested solr)
         */
        public ListProvider getListProvider();

        /**
         * Collect the specified term and query values.
         *
         * @param field The field (used to group like fields together for OR conditions).
         *
         * @param values The query values to append.
         */
        public void term(String field, Set<String> values);

    }
}
