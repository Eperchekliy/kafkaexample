package com.example.hbase;

import com.google.common.collect.Sets;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.Set;

@XmlRootElement
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
@XmlSeeAlso(value = {
        ListEntityListManagerReference.class,
        ListEntityStaticDefinition.class,
        ListEntitySolrGenericList.class
})
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME, include = com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY, property = "_")
@com.fasterxml.jackson.annotation.JsonAutoDetect(creatorVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        getterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE,
        setterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE,
        fieldVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        isGetterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE)
@com.fasterxml.jackson.annotation.JsonSubTypes({
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = ListEntityListManagerReference.class, name = "lmc"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = ListEntityStaticDefinition.class, name = "manual"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = ListEntitySolrGenericList.class, name = "list"),
})
@org.codehaus.jackson.annotate.JsonTypeInfo(use = org.codehaus.jackson.annotate.JsonTypeInfo.Id.NAME, include = org.codehaus.jackson.annotate.JsonTypeInfo.As.PROPERTY, property = "_")
@org.codehaus.jackson.annotate.JsonAutoDetect(creatorVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        getterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE,
        setterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE,
        fieldVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        isGetterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE)
@org.codehaus.jackson.annotate.JsonSubTypes({
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = ListEntityListManagerReference.class, name = "lmc"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = ListEntityStaticDefinition.class, name = "manual"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = ListEntitySolrGenericList.class, name = "list"),
})
public abstract class ListEntityReference {

    /**
     * The unique ID of the specified list.
     */
    public String id;

    /**
     * The name of the specified list.
     */
    public String name;

    /**
     * The type of list.
     */
    public ListType type;

    /**
     * The specific list of touchpoint targets to include for targeting advertisements.
     */
    public TouchpointTarget list;

    /**
     * The estimated rough count of items in the list.
     */
    public Integer count;

    /**
     * The user who owns this particular list and created it.
     */
    public UserEntity user;

    /**
     * The external identifier for the specified list.
     */
    public String external_id;

    /**
     * The status of the list in question.
     */
    public String status;

    /**
     * The terms being specified by the user for manual entry.
     */
    public Set<String> rows = Sets.newHashSet();
}
