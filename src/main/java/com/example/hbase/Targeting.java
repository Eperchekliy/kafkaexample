package com.example.hbase;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown=true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown=true)
public class Targeting {

    /**
     * Specifies what geographical locations are targeted. This attribute is optional.
     */
    public LocationTargeting locations = new LocationTargeting();

    /**
     * Specifies the networks that are targeted by the promotion. This attribute is optional.
     */
    public NetworkTargeting networks = new NetworkTargeting();

    /**
     * Specifies specific touchpoints that are targeted by the promotion. This attribute is optional. The primary
     * use case for using this field is to narrow down specific groupings of touchpoints within a display network, such
     * as by category, banner, etc.
     */
    public TouchpointTargeting touchpoints = new TouchpointTargeting();

    /**
     * The schedule used when targeting to determine the promotions schedule and
     * if it is applicable to serve.
     */
    public ScheduleTargeting schedule = new ScheduleTargeting();

    /**
     * Evaluates the specified targets on this and if any are false will prevent from serving.
     *
     * @param context The context of the targeting to perform.
     *
     * @return Returns true if the targeting criteria are valid.
     */
    public boolean eval(AdvertisementContext context) {

        if (this.networks != null) {
            if (!this.networks.eval(context)) {
                return false;
            }
        }

        if (this.touchpoints != null) {
            if (!this.touchpoints.eval(context)) {
                return false;
            }
        }

        if (this.locations != null) {
            if (!this.locations.eval(context)) {
                return false;
            }
        }

        if (this.schedule != null) {
            if (!this.schedule.eval(context)) {
                return false;
            }
        }

        return true;
    }
}
