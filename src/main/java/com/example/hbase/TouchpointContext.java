package com.example.hbase;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.map.annotate.JsonSerialize(include = org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion.NON_NULL)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.CUSTOM, include = com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY, property = "channel_type", visible = true)
@com.fasterxml.jackson.annotation.JsonSubTypes({
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = TouchpointStoreContext.class, name = "store"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = TouchpointDigitalContext.class, name = "web"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = TouchpointMobileContext.class, name = "mobile")
})
@org.codehaus.jackson.annotate.JsonTypeInfo(use = org.codehaus.jackson.annotate.JsonTypeInfo.Id.CUSTOM, include = org.codehaus.jackson.annotate.JsonTypeInfo.As.PROPERTY, property = "channel_type")
@org.codehaus.jackson.annotate.JsonSubTypes({
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = TouchpointStoreContext.class, name = "store"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = TouchpointDigitalContext.class, name = "web"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = TouchpointMobileContext.class, name = "mobile")
})
@org.codehaus.jackson.map.annotate.JsonTypeIdResolver(TouchpointContextCodehausJacksonResolver.class)
@com.fasterxml.jackson.databind.annotation.JsonTypeIdResolver(TouchpointContextFasterXmlJacksonResolver.class)
public abstract class TouchpointContext {

    private static final String REGEX_LATLONG = "^(\\-?\\d+(\\.\\d+)?),\\s*(\\-?\\d+(\\.\\d+)?)$";

    public TouchpointContext() {

    }

    public TouchpointContext(String country_code, String network_id) {
        this.country_code = country_code;
        this.network_id = network_id;
    }

    /**
     * The channel by which this touchpoint is making a request for content.
     */
    @JsonTypeId
    @XmlElement(required = true)
    public ChannelType channel_type;

    /**
     * The country code for this particular touchpoint.
     */
    @XmlElement(required = true)
    public String country_code;

    /**
     * The latitude and longitude in the format of 'lat,lon'.
     */
    public Double latitude;

    /**
     * The longitude for the current touchpoint.
     */
    public Double longitude;

    /**
     * The network ID of the touchpoint.
     */
    @XmlElement(required = true)
    public String network_id;

    /**
     * The unique site ID of the touchpoint.
     */
    @XmlElement(required = true)
    public String site_id;

    /**
     * Optional banner id associated to this touchpoint.
     */
    public String banner_id;

    /**
     * A collection of attributes specified to this touchpoint for filtering
     * and targeting of placements and ads.
     */
    public Map<String, String> attributes = new HashMap<>();

    public void setLatLong(String lat_long) {
        if (StringUtils.isNotBlank(lat_long) && lat_long.trim().matches(REGEX_LATLONG)) {
            String[] pair = lat_long.split(",");
            this.latitude = Double.parseDouble(pair[0]);
            this.longitude = Double.parseDouble(pair[1]);
        }
    }

    public boolean isLabStore() {
        boolean rc = false;
        if(attributes.containsKey("STORE_NAME")) {
            String value = attributes.get("STORE_NAME");
            if(value.trim().toUpperCase().equals("LAB STORE")) {
                rc = true;
            }
        }
        return rc;
    }

    @Override
    public String toString() {
        return this.compositeKey();
    }

    public String compositeKey() {
        return (country_code + ":" + network_id + ":" + site_id).toUpperCase();
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TouchpointContext) {
            TouchpointContext that = (TouchpointContext)obj;
            if (that.compositeKey().equalsIgnoreCase(this.compositeKey())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.compositeKey().hashCode();
    }
}
