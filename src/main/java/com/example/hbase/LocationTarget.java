package com.example.hbase;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.Set;

@XmlRootElement
@XmlSeeAlso(value = {
        LocationCityTarget.class,
        LocationPostalCodeTarget.class,
        LocationAttributeTarget.class,
        LocationWktPolygonTarget.class,
        LocationTouchpointProximityTarget.class,
        LocationCountryTarget.class
})
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown=true)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME, include = com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY, property = "type")
@com.fasterxml.jackson.annotation.JsonSubTypes({
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = LocationCityTarget.class, name = "city"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = LocationPostalCodeTarget.class, name = "postal"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = LocationWktPolygonTarget.class, name = "wkt"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = LocationAttributeTarget.class, name = "attribute"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = LocationTouchpointProximityTarget.class, name = "touchpoint-proximity"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = LocationCountryTarget.class, name = "country"),
})
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown=true)
@org.codehaus.jackson.annotate.JsonTypeInfo(use = org.codehaus.jackson.annotate.JsonTypeInfo.Id.NAME, include = org.codehaus.jackson.annotate.JsonTypeInfo.As.PROPERTY, property = "type")
@org.codehaus.jackson.annotate.JsonSubTypes({
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = LocationCityTarget.class, name = "city"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = LocationPostalCodeTarget.class, name = "postal"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = LocationWktPolygonTarget.class, name = "wkt"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = LocationAttributeTarget.class, name = "attribute"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = LocationTouchpointProximityTarget.class, name = "touchpoint-proximity"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = LocationCountryTarget.class, name = "country"),
})
public abstract class LocationTarget implements TargetCondition {

    /**
     * The unique identifier for this location target.
     */
    public String id;

    /**
     * Given the specified advertisements context return true if this location matches.
     *
     * @param context The context of the advertisement request
     * @return
     */
    public abstract boolean eval(AdvertisementContext context);

    public abstract void buildQuery(QueryBuilder qb, TargetedLocationProvider locations);

    public static interface QueryBuilder {
        public void term(String field, Set<String> values);
        public void polygon(Set<String> values);
    }
}
