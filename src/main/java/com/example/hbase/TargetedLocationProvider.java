package com.example.hbase;

import java.util.Set;

public interface TargetedLocationProvider {

    /**
     * Optional usage that will set a cached instance of the provider for delegation purposes. The primary
     * purpose here is that we have intermixed dependencies and common lookups between
     * getting touchpoints and getting targeting criteria. (For example loading all matching stores
     * and doing an inline mix).
     *
     * TODO This is a bad code smell... but for now we'll deal with it.
     *
     * @param cache The cache provider to inject.
     */
    void setCache(TargetedLocationProvider cache);

    /**
     * Returns all touchpoints that match the specified location targeting criteria. Used
     * for display of sites that match the specified criteria.
     * @param targeting The targeting locations to restrict touchpoints against
     * @return Returns all touchpoints that match the specified location attributes.
     */
    Set<TouchpointContext> getTouchpoints(LocationTargeting targeting);

    /**
     * Returns all touchpoints that match the specified touchpoint targeting criteria. Used
     * for display of sites that match the specified criteria.
     * @param targeting The targeting targets to restrict touchpoints against
     * @return Returns all touchpoints that match the specified location attributes.
     */
    Set<TouchpointContext> getTouchpoints(TouchpointTargeting targeting);

    /**
     * Given the combined targeting criteria return the intersection of all touchpoints
     * that match the specified criteria.
     * @param locations The criteria to retrieve touchpoints against
     * @param touchpoints The criteria to retrieve touchpoints against.
     * @return Returns the touchpoints that intersect both targeting conditions.
     */
    Set<TouchpointContext> getTouchpoints(LocationTargeting locations, TouchpointTargeting touchpoints);

    /**
     * Returns the distinct collection of networks that match the specified locations and touchpoints.
     *
     * @param locations The criteria to retrieve touchpoints against
     * @param touchpoints The criteria to retrieve touchpoints against.
     *
     * @return Returns the unique collection of networks that intersect both targeting conditions.
     */
    Set<NetworkReference> getNetworks(TouchpointTargeting touchpoints);

    /**
     * Given the combined targeting criteria return the intersection of all touchpoints
     * that match the specified criteria.
     * @param locations The criteria to retrieve touchpoints against
     * @param touchpoints The criteria to retrieve touchpoints against.
     * @return Returns the touchpoints that intersect both targeting conditions.
     */
    Long getTouchpointCount(LocationTargeting locations, TouchpointTargeting touchpoints);
}
