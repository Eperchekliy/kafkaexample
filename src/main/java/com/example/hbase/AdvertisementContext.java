package com.example.hbase;

import java.util.List;
import java.util.Set;

public interface AdvertisementContext {

    /**
     * The globally unique session ID for this advertisement. Used for creating and managing session IDs across all channels, devices, and touchpoints for the entire world.
     *
     * @return The guaranteed universally unique session ID.
     */
    String sessionId();

    /**
     * The 360 degree profile associated to the current advertisement context. This is the individual consumer/shopper/user that is the receiver of ad content.
     *
     * @return The consumer currently associated and there entire 360 profile.
     */
    Consumer getProfile();

    /**
     * The 360 degree household associated to the current advertisement context. This is the aggregation of the individual consumer to the household level. If the consumer is in a household of "1" than household is simply the same profile as that provided by profile.
     *
     * @return The consumer currently associated and there entire 360 profile.
     */
    Consumer getHousehold();

    /**
     * The context of the touchpoint that the consumer is interacting with.
     *
     * @return The touchpoint context.
     */
    TouchpointContext getTouchpoint();

    /**
     * The network that the user is accessing content for. This can differ from touchpoint in that we could be using a touchpoint in a different network to access another networks media. In 99% of the occasions this will match the touchpoint's network.
     *
     * @return
     */
    NetworkObject getNetwork();

    /**
     * The channel that the user is making a request to get content for. This is exactly the same as touchpoint, however it is provided for ease of access.
     *
     * @return The touchpoints channel (same as getTouchpoint().channel_type).
     */
    ChannelType getChannelType();

    /**
     * Gets any and all POS messages associated to the current user. This is provided when the user is accessing content from a store care of our store systems. This may need to move to store touchpoint context.
     *
     * @return
     */
    List<TRAN_MSG> getPosMessages();

    /**
     * Limits the content to be served to a specific set, if none is provided assume all media from all systems is available to be served. Useful to only different certain types of experiences or media. For now this is used to only deliver "BLIP" media, in later releases this will likely be left blank.
     *
     * @return The content types to restrict down to.
     */
    Set<String> getContentTypes();

    /**
     * Returns any prior media deliveries given to the user in the same session so that we do not over-deliver content to a user.
     *
     * @return Returns any and all media given in the same session to allow targeting to remove it and avoid analytic capturing.
     */
    Set<String> getMediaDeliveries();

    /**
     * Update media deliveries to filter our already existed media.
     *
     * @param promotion_ids
     *            The ids to register as having already been delivered.
     */
    void addMediaDeliveries(Set<String> promotion_ids);

    /**
     * Gets a cache key that can be used for this context when attempting to optimize for placements, templates, and other features that need to be looked up and we want to avoid burdening the system with asking for data over and over again.
     *
     * @return Returns a cache key that can be used for lookup objects and references.
     */
    String getCacheKey();

    /**
     * The the local timezone given the current touchpoint context.
     *
     * @return
     */
    DateTime getContextLocalizedTime(DateTime utcTime);

    /**
     * Trace specific events and allow remote debug command to slow informational messages back to caller for debug and diagnostics.
     *
     * @param message
     *            The message format string
     * @param args
     *            The arguments to pass into the specified string
     */
    void debug(String message, Object... args);

    /**
     * Returns true if the request context had debugging enabled.
     *
     * @return Returns true if the request context enabled tracing/debugging.
     */
    boolean isDebug();

    /**
     * Given the provided advertising context convert the specified
     * integer base10 currency value to it's decimal form. For example;
     * if the country is Japan, the base10 of the integer currency value 100, is 100.00
     * as YEN has no decimal places. However, if the country is USA, the int currency
     * value of 100 is 1.00.
     *
     * @param value
     *            The value of the currency in base10 format (USA 1.00 = 100).
     *
     * @return Return the normalized decimal value form of the currency.
     */
    double currencyToDecimalNotation(Integer currency_in_base10);

    /**
     * Returns the original object that was sent to the server for this request.
     *
     * @return Returns the original data of this request.
     */
    Object getOriginalRequest();
}
