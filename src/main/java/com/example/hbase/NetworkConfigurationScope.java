package com.example.hbase;

public enum NetworkConfigurationScope {
    Country,
    Network;

    @com.fasterxml.jackson.annotation.JsonCreator
    @org.codehaus.jackson.annotate.JsonCreator
    public static NetworkConfigurationScope fromString(String key) {
        for(NetworkConfigurationScope type : NetworkConfigurationScope.values()) {
            if(type.name().equalsIgnoreCase(key)) {
                return type;
            }
        }
        return null;
    }

}
