package com.example.hbase;

public interface NetworkProvider {

    /**
     * Given a specified chain and store within a specific country, get the network that this
     * touchpoint represents.
     *
     * @param country_code The country code of the chain to recieve.
     * @param chain_number The chain number to lookup
     * @param store_number The store number to lookup
     * @return Returns the network that matches the specified chain and store number.
     */
    Network getNetworkByChain(String country_code, String chain_number, String store_number);

    /**
     * Given the specified network id and site ID we lookup the touchpoint.
     *
     * @param country_code The ISO3 country code of the network being looked up.
     * @param network_id The network id.
     * @param site_id The site id
     * @return Returns the specified network by it's network id and site id.
     */
    Network getNetworkByTouchpoint(String country_code, String network_id, String site_id, ChannelType channel_type);

    /**
     * Gets the configured settings for the specified network and country that the network is within.
     *
     * @param country_code The country code of the network
     * @param network_id The ID of the network
     * @param channel The channel the network is requesting operations against
     * @return The configured rate limits for the network in question.
     */
    NetworkSettings getNetworkSettings(String country_code, String network_id);
}

