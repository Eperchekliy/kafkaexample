package com.example.hbase;

public interface TargetCondition {


    public String getConditionGroupKey();


    boolean eval(AdvertisementContext context);

}
