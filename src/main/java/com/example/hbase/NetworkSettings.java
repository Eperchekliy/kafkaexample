package com.example.hbase;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkSettings {

    /**
     * Rules specific to a particular network in terms of filtering out
     * content or otherwise modifying or re-prioritizing orders.
     */
    public List<NetworkConfiguration> rules = new ArrayList<NetworkConfiguration>();

    /**
     * Finds and returns all configured rules of this type.
     *
     * @param ctx
     * @param clazz
     * @return
     */
    public <T extends NetworkConfiguration> List<T> findActiveConfigurations(final AdvertisementContext ctx, Class<T> clazz) {
        if (rules != null) {
            // given all configuration options walk them to find our specific
            // rules within a given network
            Predicate<T> keep_predicate = new Predicate<T>() {
                @Override
                public boolean apply(T t) {
                    return !t.disabled && (t.targeting == null || t.targeting.eval(ctx));
                }
            };
            return Lists.newArrayList(Iterables.filter(Iterables.filter(rules, clazz), keep_predicate));
        }
        return new ArrayList<>();
    }

    /**
     * Finds and returns the first configured rule of this type, or null if no rules of this type are configured;
     *
     * @param ctx
     * @param clazz
     * @return
     */
    public <T extends NetworkConfiguration> T findActiveConfiguration(AdvertisementContext ctx, Class<T> clazz) {
        List<T> configs = findActiveConfigurations(ctx, clazz);
        if (!configs.isEmpty())
            return configs.get(0);
        return null;
    }
}
