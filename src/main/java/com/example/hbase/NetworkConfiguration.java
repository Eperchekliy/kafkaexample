package com.example.hbase;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.UUID;

@XmlRootElement
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
@com.fasterxml.jackson.annotation.JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME, include = com.fasterxml.jackson.annotation.JsonTypeInfo.As.PROPERTY, property = "type")
@com.fasterxml.jackson.annotation.JsonAutoDetect(creatorVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        getterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE,
        setterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE,
        fieldVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        isGetterVisibility = com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE)
@com.fasterxml.jackson.annotation.JsonSubTypes({
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = LongTermControlConfiguration.class, name = "long-term-controls"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = BrandLoyaltyProtectionConfiguration.class, name = "brand-loyalty-potection"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = ProductCarriedRuleConfiguration.class, name = "product-carried"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = ThirdPartyConsumerApprovalConfiguration.class, name = "consumer-approval"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = OrderTotalProductExclusionConfiguration.class, name = "order-total-product-exclusion"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = SensitiveCategoryConfiguration.class, name = "sensitive-category"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = LogoConfiguration.class, name = "logo"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = PerSessionPrintConfiguration.class, name = "print-experience-management"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = NetworkBarcodeConfiguration.class, name = "barcode"),
        @com.fasterxml.jackson.annotation.JsonSubTypes.Type(value = MyFavoriteDealsConfiguration.class, name = "my-favorite-deals"),
})
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonTypeInfo(use = org.codehaus.jackson.annotate.JsonTypeInfo.Id.NAME, include = org.codehaus.jackson.annotate.JsonTypeInfo.As.PROPERTY, property = "type")
@org.codehaus.jackson.annotate.JsonAutoDetect(creatorVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        getterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE,
        setterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE,
        fieldVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.PUBLIC_ONLY,
        isGetterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE)
@org.codehaus.jackson.annotate.JsonSubTypes({
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = LongTermControlConfiguration.class, name = "long-term-controls"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = BrandLoyaltyProtectionConfiguration.class, name = "brand-loyalty-potection"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = ProductCarriedRuleConfiguration.class, name = "product-carried"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = ThirdPartyConsumerApprovalConfiguration.class, name = "consumer-approval"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = OrderTotalProductExclusionConfiguration.class, name = "order-total-product-exclusion"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = SensitiveCategoryConfiguration.class, name = "sensitive-category"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = LogoConfiguration.class, name = "logo"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = PerSessionPrintConfiguration.class, name = "print-experience-management"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = NetworkBarcodeConfiguration.class, name = "barcode"),
        @org.codehaus.jackson.annotate.JsonSubTypes.Type(value = MyFavoriteDealsConfiguration.class, name = "my-favorite-deals"),
})
public abstract class NetworkConfiguration {

    /**
     * The unique identifier for this specific network configuration.
     */
    public String id = UUID.randomUUID().toString();

    /**
     * A free form text field naming this particular configuration.
     */
    public String name;

    /**
     * If this rule has been disabled.
     */
    public boolean disabled = false;

    /**
     * Any targeting applicable for this network configuration that
     * will restrict it's scope.
     */
    public Targeting targeting;

    /**
     * The scope for the specified configuration, used when merging country and network configuration
     * together.
     */
    public NetworkConfigurationScope scope = NetworkConfigurationScope.Network;
}
