package com.example.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
@PropertySource("classpath:application.properties")
public class HBaseClient extends AbstractHBaseClient {

    @Value("${hbase.zookeeper}")
    private String zookeeper;

    private Configuration config;
    private Connection connection;
    private Admin admin;
    private HbaseTemplate hbaseTemplate;

    @PostConstruct
    public void init() {
        config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.quorum", zookeeper);
        createConnection();
        createAdmin();
        createHBaseTemplate();
    }

    private void createConnection() {
        try {
            connection = ConnectionFactory.createConnection(config);
        } catch (IOException e) {
            throw new RuntimeException("Unable to connect to Hbase instance, check inner error for details", e);
        }
    }

    private void createAdmin() {
        try {
            admin = connection.getAdmin();
        } catch (IOException e) {
            throw new RuntimeException("Unable to connect to Hbase instance, check inner error for details", e);
        }
    }

    private void createHBaseTemplate() {
        hbaseTemplate = new HbaseTemplate(config);
    }

    @Override
    public HbaseTemplate getHbaseTemplate() {
        return hbaseTemplate;
    }

    @Override
    public Admin getAdmin() {
        return admin;
    }
}
