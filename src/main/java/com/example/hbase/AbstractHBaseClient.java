package com.example.hbase;

import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.springframework.data.hadoop.hbase.HbaseTemplate;
import org.springframework.data.hadoop.hbase.RowMapper;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public abstract class AbstractHBaseClient {

    public byte[] get(String tableName, String rowKey, String columnFamily) {
        return getHbaseTemplate().get(tableName, rowKey, columnFamily, (result, rowNum) -> result.value());
    }

    public <T> T get(String tableName, String rowKey, String columnFamily, RowMapper<T> rowMapper) {
        return getHbaseTemplate().get(tableName, rowKey, columnFamily, rowMapper);
    }

    public byte[] get(String tableName, String rowKey, String columnFamily, String qualifier) {
        return getHbaseTemplate().get(tableName, rowKey, columnFamily, qualifier, (result, rowNum) -> result.value());
    }

    public byte[] get(String tableName, String rowKey, String columnFamily, String qualifier, int maxVersions) {
        return getHbaseTemplate().execute(tableName, table -> {
            Get get = new Get(rowKey.getBytes());
            get.addColumn(columnFamily.getBytes(), qualifier.getBytes());
            get.setMaxVersions(maxVersions);

            return table.get(get).value();
        });
    }

    public <T> T get(String tableName, String rowKey, String columnFamily, int maxVersions, RowMapper<T> rowMapper) {
        return getHbaseTemplate().execute(tableName, table -> {
            Get get = new Get(rowKey.getBytes());
            get.addFamily(columnFamily.getBytes());
            get.setMaxVersions(maxVersions);

            Result result = table.get(get);
            return rowMapper.mapRow(result, 0);
        });
    }


    public void put(String tableName, String rowKey, String columnFamily, String qualifier, byte[] value) {
        getHbaseTemplate().put(tableName, rowKey, columnFamily, qualifier, value);
    }

    public void put(String tableName, final String rowKey, final String columnFamily, final Map<String, String> values) {
        getHbaseTemplate().execute(tableName, table -> {
            Put put = new Put(rowKey.getBytes());
            values.entrySet().forEach(x -> put.addColumn(columnFamily.getBytes(), x.getKey().getBytes(), x.getValue().getBytes()));
            table.put(put);
            return null;
        });
    }

    public void put(String tableName, final String rowKey, final String columnFamily, String qualifier, byte[] value, Durability durability) {
        getHbaseTemplate().execute(tableName, table -> {
            Put put = new Put(rowKey.getBytes());
            put.addColumn(columnFamily.getBytes(), qualifier.getBytes(), value);
            put.setDurability(durability);

            table.put(put);
            return null;
        });
    }

    public boolean checkAndPut(String tableName, final String rowKey, final String columnFamily, String qualifier, byte[] value, byte[] expectedValue, Durability durability) {
        return getHbaseTemplate().execute(tableName, table -> {
            Put put = new Put(rowKey.getBytes());
            put.addColumn(columnFamily.getBytes(), qualifier.getBytes(), value);
            put.setDurability(durability);

            return table.checkAndPut(rowKey.getBytes(), columnFamily.getBytes(), qualifier.getBytes(), expectedValue, put);
        });
    }

    public void delete(String tableName, String rowKey, String columnFamily) {
        getHbaseTemplate().delete(tableName, rowKey, columnFamily);
    }

    public void delete(String tableName, String rowKey, Durability durability) {
        getHbaseTemplate().execute(tableName, table -> {
            Delete delete = new Delete(rowKey.getBytes());
            delete.setDurability(durability);

            table.delete(delete);
            return null;
        });
    }

    public List<byte[]> list(String tableName, Scan scan) {
        return list(tableName, scan, (result, rowNum) -> result.value());
    }

    public <T> List<T> list(String tableName, Scan scan, RowMapper<T> rowMapper) {
        return getHbaseTemplate().find(tableName, scan, rowMapper);
    }

    public void increment(String tableName, String rowKey, String columnFamily, String qualifier) {
        getHbaseTemplate().execute(tableName, table -> {
            Increment increment = new Increment(rowKey.getBytes());
            increment.addColumn(columnFamily.getBytes(), qualifier.getBytes(), 1);

            table.increment(increment);
            return null;
        });
    }

    public void increment(String tableName, String rowKey, String columnFamily, String qualifier, long amount) {
        getHbaseTemplate().execute(tableName, table -> {
            Increment increment = new Increment(rowKey.getBytes());
            increment.addColumn(columnFamily.getBytes(), qualifier.getBytes(), amount);

            table.increment(increment);
            return null;
        });
    }

    public void shutdown() throws IOException {
        getAdmin().shutdown();
    }

    public void flush(String tableName) throws IOException {
        getAdmin().flush(TableName.valueOf(tableName));
    }

    public abstract HbaseTemplate getHbaseTemplate();

    public abstract Admin getAdmin();
}
