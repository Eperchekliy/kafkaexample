package com.example.hbase;

public interface ListProvider {

    /**
     * Given the specified reference, attempt to update the referenced objects `rows` property
     * with the latest values, or not, depending on restrictions of performance.
     *
     * @param ref The reference to the list to process and update.
     */
    void update(ListEntityReference ref);
}
