package com.example.hbase;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;

public class HbaseNetworkProvider implements NetworkProvider, NetworkRepository {

    private final String tableName;

    public static class Schema {
        public static final String COLUMN_FAMILY = "c";

        public static class Columns {
            public static final String NETWORK_ID = "network_id";
            public static final String NETWORK_NAME = "network_name";
            public static final String SITE_ID = "site_id";
            public static final String SITE_NAME = "site_name";
            public static final String PARENT_CORP_KEY = "parent_corp_key";
            public static final String PARENT_CORP_NAME = "parent_corp_name";
            public static final String COUNTRY_CODE = "country_code";
            public static final String JSON_DOC = "doc";
        }

        public static final byte[][] Qualifiers = new byte[][]{
                Columns.NETWORK_ID.getBytes(),
                Columns.NETWORK_NAME.getBytes(),
                Columns.SITE_ID.getBytes(),
                Columns.SITE_NAME.getBytes(),
                Columns.PARENT_CORP_KEY.getBytes(),
                Columns.PARENT_CORP_NAME.getBytes(),
                Columns.COUNTRY_CODE.getBytes()
        };

        public static byte[][] rows(Network network, ChannelType channel) {
            byte[][] data = {
                    Bytes.toBytes(network.getNetworkId()),
                    Bytes.toBytes(network.getNetworkName()),
                    Bytes.toBytes(network.getSiteId()),
                    Bytes.toBytes(network.getSiteName()),
                    Bytes.toBytes(network.getParentCorpKey()),
                    Bytes.toBytes(network.getParentCorpName()),
                    Bytes.toBytes(network.getCountryCode())
            };
            return data;
        }
    }

    @Autowired
    private HBaseClient hClient;

    public HbaseNetworkProvider(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public Network getNetworkByChain(String country_code, String chain_number, String store_number) {
        if (Integer.valueOf(store_number) > 10000) {
            store_number = String.valueOf((Integer.valueOf(store_number) - (Integer.valueOf(chain_number) * 10000)));
        }
        return getNetwork(country_code, "CHAIN:" + chain_number + ":" + store_number);
    }

    @Override
    public Network getNetworkByTouchpoint(String country_code, String network_id, String site_id, ChannelType channel_type) {
        return getNetwork(country_code, "TOUCHPOINT:" + network_id + ":" + site_id + ":" + channel_type);
    }

    private Network getNetwork(final String country_code, String key) {

        return hClient.get(tableName, (country_code + ":" + key).toUpperCase(), Schema.COLUMN_FAMILY, (result, rowNum) -> {
            Record r = new Record();

            r.network_id = Bytes.toString(result.getValue(Bytes.toBytes(Schema.COLUMN_FAMILY), Bytes.toBytes(Schema.Columns.NETWORK_ID)));
            r.network_name = Bytes.toString(result.getValue(Bytes.toBytes(Schema.COLUMN_FAMILY), Bytes.toBytes(Schema.Columns.NETWORK_NAME)));
            r.site_id = Bytes.toString(result.getValue(Bytes.toBytes(Schema.COLUMN_FAMILY), Bytes.toBytes(Schema.Columns.SITE_ID)));
            r.site_name = Bytes.toString(result.getValue(Bytes.toBytes(Schema.COLUMN_FAMILY), Bytes.toBytes(Schema.Columns.SITE_NAME)));
            r.parent_corp_key = Bytes.toString(result.getValue(Bytes.toBytes(Schema.COLUMN_FAMILY), Bytes.toBytes(Schema.Columns.PARENT_CORP_KEY)));
            r.parent_corp_name = Bytes.toString(result.getValue(Bytes.toBytes(Schema.COLUMN_FAMILY), Bytes.toBytes(Schema.Columns.PARENT_CORP_NAME)));
            r.country_code = _.coalesce(Bytes.toString(result.getValue(Bytes.toBytes(Schema.COLUMN_FAMILY), Bytes.toBytes(Schema.Columns.COUNTRY_CODE))), country_code, "USA");

            return r.build();
        });
    }


    private class Record {
        public String network_id;
        public String network_name;
        public String site_id;
        public String site_name;
        public String parent_corp_key;
        public String parent_corp_name;
        public String country_code;

        public Network build() {
            return Network.newBuilder()
                    .setNetworkId(network_id)
                    .setNetworkName(network_name)
                    .setSiteId(site_id)
                    .setSiteName(site_name)
                    .setParentCorpKey(parent_corp_key)
                    .setParentCorpName(parent_corp_name)
                    .setCountryCode(country_code)
                    .build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void create(Network network, ChannelType channelType) {
        if (network == null) {
            throw new RuntimeException("Network Null");
        }
        if (channelType == null) {
            throw new RuntimeException("Channel Type Null");
        }

        String key = (network.getNetworkId() + ":" + network.getSiteId() + ":" + channelType).toUpperCase();
        Network match = getNetwork(network.getCountryCode(), key);
        if (match != null) {
            return;
        }
        String row_id = (network.getCountryCode() + ":" + key).toUpperCase();

        ImmutableMap<String, String> values = new ImmutableMap.Builder<String, String>()
                .put(Schema.Columns.NETWORK_ID, network.getNetworkId())
                .put(Schema.Columns.NETWORK_NAME, network.getNetworkName())
                .put(Schema.Columns.SITE_ID, network.getSiteId())
                .put(Schema.Columns.SITE_NAME, network.getSiteName())
                .put(Schema.Columns.PARENT_CORP_KEY, network.getParentCorpKey())
                .put(Schema.Columns.PARENT_CORP_NAME, network.getParentCorpName())
                .put(Schema.Columns.COUNTRY_CODE, network.getCountryCode())
                .build();

        try {
            hClient.put(tableName, row_id, Schema.COLUMN_FAMILY, values);
        } catch (Exception e) {
            throw new RuntimeException("Unable to add network due to hbase error", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NetworkSettings getNetworkSettings(String country_code, String network_id) {
        String key = (country_code + ":" + network_id + ":SETTINGS").toUpperCase();
        NetworkSettings network_level_settings = fetch(key);

        // now append the country specific settings
        String country_key = (country_code + ":COUNTRY:SETTINGS").toUpperCase();
        NetworkSettings country_level_settings = fetch(country_key);

        NetworkSettings union = new NetworkSettings();
        if (country_level_settings != null && country_level_settings.rules != null && !country_level_settings.rules.isEmpty()) {

            country_level_settings.rules.forEach(cfg -> {
                cfg.scope = NetworkConfigurationScope.Country;
                union.rules.add(cfg);
            });
        }
        if (network_level_settings != null && network_level_settings.rules != null && !network_level_settings.rules.isEmpty()) {

            network_level_settings.rules.forEach(cfg -> {
                cfg.scope = NetworkConfigurationScope.Network;
                union.rules.add(cfg);
            });

        }
        return union;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateNetworkSettings(String country_code, String network_id, NetworkSettings settings) {
        String key = (country_code + ":" + network_id + ":SETTINGS").toUpperCase();
        String json = _.toJson(settings);

        // remove from settings any country specific settings
        Iterables.removeIf(settings.rules, input -> input.scope != NetworkConfigurationScope.Network);

        hClient.put(tableName, key, Schema.COLUMN_FAMILY, Schema.Columns.JSON_DOC, Bytes.toBytes(json));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateCountrySettings(String country_code, NetworkSettings settings) {
        String key = (country_code + ":COUNTRY:SETTINGS").toUpperCase();
        String json = _.toJson(settings);

        // remove from settings any network specific settings from country configuration
        Iterables.removeIf(settings.rules, input -> input.scope != NetworkConfigurationScope.Country);

        hClient.put(tableName, key, Schema.COLUMN_FAMILY, Schema.Columns.JSON_DOC, Bytes.toBytes(json));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NetworkSettings getCountrySettings(String country_code) {
        String key = (country_code + ":COUNTRY:SETTINGS").toUpperCase();
        return fetch(key);
    }

    private NetworkSettings fetch(String rowkey) {
        byte[] result = hClient.get(tableName, rowkey, Schema.COLUMN_FAMILY, Schema.Columns.JSON_DOC);

        NetworkSettings settings = new NetworkSettings();

        if (result.length == 0) {
            return settings;
        }

        return _.fromJson(Bytes.toString(result), NetworkSettings.class);
    }
}

