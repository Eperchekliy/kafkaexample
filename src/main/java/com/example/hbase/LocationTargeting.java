package com.example.hbase;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown=true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown=true)
public class LocationTargeting implements TargetCondition {

    /**
     * The geographical locations being targeted.
     */
    public List<LocationTarget> include = new ArrayList<>();

    /**
     * The geographical locations to exclude from targeting.
     */
    public List<LocationTarget> exclude = new ArrayList<>();

    public boolean eval(AdvertisementContext context) {
        // with include ANY match to include indicates success (you can be in 1 of 100, and still be good)
        if (this.include != null && this.include.isEmpty() == false) {
            boolean match = false;  // set to false by default if provided
            for (TargetCondition t : this.include) {
                if (t.eval(context)) {
                    match = true;
                    break;
                }
            }
            // no matches for include, can't process
            if (false == match) {
                return match;
            }
        }

        // with exclude ANY match to exclude for positive, is a failure (you can be NOT in 99 of 100, and having 1 match means no)
        if (this.exclude != null && this.exclude.isEmpty() == false) {
            for (TargetCondition t : this.exclude) {
                if (t.eval(context)) {
                    return false;   // we found a match on exclude we can fast exit
                }
            }
        }
        return true;    // true by default when no exclude or include exists or not excluded
    }

    @Override
    public String getConditionGroupKey() {
        return "locations";
    }

}
