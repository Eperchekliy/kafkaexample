package com.example.hbase;

public interface NetworkRepository {

    /**
     * Creates the specified network.
     *
     * @param network The network to create
     * @param channelType The channel type to affilate
     */
    void create(Network network, ChannelType channelType);

    /**
     * Updates the settings for the specified network (and only those settings scoped to network).
     *
     * @param country_code The country code of the network
     * @param network_id The network ID to update
     * @param settings The settings to update.
     */
    void updateNetworkSettings(String country_code, String network_id, NetworkSettings settings);

    /**
     * Sets and applies country specified configuration that is applicable to all networks
     * that are rooted in the specified country.
     *
     * @param country_code The country code to update it's settings against
     * @param settings The settings to update for the specified country.
     */
    void updateCountrySettings(String country_code, NetworkSettings settings);

    /**
     * Returns the country only settings configured in the system.
     *
     * @param country_code The country code to get settings against.
     *
     * @return Returns an empty network settings or the actual configuration if found.
     */
    NetworkSettings getCountrySettings(String country_code);

    /**
     * Gets the configured settings for the specified network and country that the network is within.
     *
     * @param country_code The country code of the network
     * @param network_id The ID of the network
     * @param channel The channel the network is requesting operations against
     * @return The configured rate limits for the network in question.
     */
    NetworkSettings getNetworkSettings(String country_code, String network_id);
}
