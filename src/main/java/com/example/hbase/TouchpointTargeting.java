package com.example.hbase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@XmlRootElement
@com.fasterxml.jackson.annotation.JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
public class TouchpointTargeting implements TargetCondition {

    private static final Logger logger = LoggerFactory.getLogger(TouchpointTargeting.class);

    /**
     * The specific list of touchpoint targets to include for targeting advertisements.
     */
    public List<TouchpointTarget> include = new ArrayList<>();

    /**
     * The specific list of touchpoint targets to exclude from serving advertisements.
     */
    public List<TouchpointTarget> exclude = new ArrayList<>();

    /**
     * Evaluates the touchpoint context alone and checks if the current targeting
     * conditions match the specified context.
     *
     * @param context The context to evaluate if it matches the current targeting conditions
     * @return Returns true if we have a match
     */
    public boolean eval(TouchpointContext context) {
        if (context == null) return false;

        if (this.include != null && this.include.isEmpty() == false) {
            boolean match = hasAtLeastOneMatch(context, this.include);
            if (!match) {
                return false;
            }
        }

        if (this.exclude != null && this.exclude.isEmpty() == false) {
            boolean match = hasAtLeastOneMatch(context, this.exclude);
            if (!match) {
                return false;
            }
        }

        return true;    // true by default when no exclude or include exists or not excluded
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean eval(AdvertisementContext context) {
        if (context == null)
            return false;

        // with include ANY match to include indicates success (you can be in 1 of 100, and still be good)
        if (this.include != null && this.include.isEmpty() == false) {
            boolean match = hasAtLeastOneMatch(context.getTouchpoint(), this.include);
            // no matches for include, can't process
            if (false == match) {
                return match;
            }
        }

        // with exclude ANY match to exclude for positive, is a failure (you can be NOT in 99 of 100, and having 1 match means no)
        if (this.exclude != null && this.exclude.isEmpty() == false) {
            boolean match = hasAtLeastOneMatch(context.getTouchpoint(), this.exclude);
            if (match) {
                return false;
            }
        }

        return true;    // true by default when no exclude or include exists or not excluded
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getConditionGroupKey() {
        return "touchpoint";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (false == obj instanceof TouchpointTargeting) {
            return false;
        }
        TouchpointTargeting that = (TouchpointTargeting)obj;
        return this.toString().equalsIgnoreCase(that.toString());
    }

    /**
     * Used for guava caching and checksums.
     * TODO on performance of this.
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        if (include != null) {
            for (TouchpointTarget t : include) {
                sb.append(Sample.hash(_.toJson(t).getBytes()));
            }
        }
        if (exclude != null) {
            for (TouchpointTarget t : exclude) {
                sb.append(Sample.hash(_.toJson(t).getBytes()));
            }
        }
        return sb.toString();
    }

    private static boolean hasAtLeastOneMatch(TouchpointContext ctx, List<TouchpointTarget> conditions) {
        HashMap<String, List<TouchpointTarget>> or_conditions = new HashMap<>();
        for (TouchpointTarget c : conditions) {
            String key = c.getConditionGroupKey();
            if (!or_conditions.containsKey(key)) {
                or_conditions.put(key, new ArrayList<TouchpointTarget>());
            }
            or_conditions.get(key).add(c);
        }

        // TODO this smells a bit like we are compensating/not trusting the code here do we not have good test coverage?
        if (logger.isTraceEnabled()) {
            logger.trace("OR CONDITIONS: ");
            for (String orc : or_conditions.keySet()) {
                logger.trace("    {}: {}", orc, _.toJson(or_conditions.get(orc)));
            }
        }

        for (String key : or_conditions.keySet()) {
            List<TouchpointTarget> list = or_conditions.get(key);
            boolean one_match = false;
            for (TouchpointTarget c : list) {
                if (c.eval(ctx)) {
                    one_match = true;
                    break;
                }
            }
            if (!one_match)
                return false;
        }

        return true;
    }
}