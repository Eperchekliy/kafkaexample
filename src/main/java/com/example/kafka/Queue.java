package com.example.kafka;

import java.io.Closeable;
import java.util.List;

public interface Queue<T> extends Closeable {

    void publish(final List<T> messages);

    void publish(final T msg);

}

