package com.example.kafka;

import java.security.MessageDigest;

public class Sample {

    /**
     * Hash the specified ID. Assumes the response is a hex based cryptographic
     * hash with set range from A-Z, and 0-9.
     *
     * Assumes.... A = 1/16th the data AA = 1/256th the data AAA = 1/4096th the
     * data AA || BB = 1/128th the data etc...
     *
     * <a
     * href="http://wiki.apache.org/solr/UniqueKey#Cryptographic_hash">http://
     * wiki.apache.org/solr/ UniqueKey#Cryptographic_hash</a>
     *
     * @param value
     *            the unique id to hash (for example user id).
     * @param length
     *            the maximum character length of the hash to return
     * @return a string of (N) length used for random sampling and bucket
     *         placement.
     */
    public static String hash(Object value, Integer length) {
        String checksum = "";
        try {
            byte[] digest = MessageDigest.getInstance("MD5").digest(value.toString().getBytes("UTF-8"));
            StringBuffer sb = new StringBuffer();
            for (int l = 0; l < digest.length; l++) {
                sb.append(Integer.toString((digest[l] & 0xff) + 0x100, 16).substring(1));
            }
            if (length != null) {
                checksum = sb.toString().substring(0, length);
            } else {
                checksum = sb.toString();
            }
        } catch (Exception e) {
            throw new RuntimeException("Could not create MD5 checksum for string '" + value + "'", e);
        }
        return checksum.toUpperCase();
    }

    /**
     * Hash the specified ID. Assumes the response is a hex based cryptographic
     * hash with set range from A-Z, and 0-9.
     *
     * Assumes.... A = 1/16th the data AA = 1/256th the data AAA = 1/4096th the
     * data AA || BB = 1/128th the data etc...
     *
     * <a
     * href="http://wiki.apache.org/solr/UniqueKey#Cryptographic_hash">http://
     * wiki.apache.org/solr/ UniqueKey#Cryptographic_hash</a>
     *
     * @param value
     *            the unique id to hash (for example user id).
     * @param length
     *            the maximum character length of the hash to return
     * @return a string of (N) length used for random sampling and bucket
     *         placement.
     */
    public static String hash(byte[] value) {
        String checksum = "";
        try {
            byte[] digest = MessageDigest.getInstance("MD5").digest(value);
            StringBuffer sb = new StringBuffer();
            for (int l = 0; l < digest.length; l++) {
                sb.append(Integer.toString((digest[l] & 0xff) + 0x100, 16).substring(1));
            }
            checksum = sb.toString();
        } catch (Exception e) {
            throw new RuntimeException("Could not create MD5 checksum for string '" + value + "'", e);
        }
        return checksum.toUpperCase();
    }

    /**
     * Will convert the specified string to an MD5 string.
     *
     * @param value The value to convert to an MD5 string
     * @return The value in MD5 checkusm format.
     */
    public static String md5(String value) {
        if (value == null) {
            throw new RuntimeException("Can not MD5 hash an empty string");
        }
        return hash(value, null).toUpperCase();
    }
}
