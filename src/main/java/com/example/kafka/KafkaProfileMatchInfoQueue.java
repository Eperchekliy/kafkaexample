package com.example.kafka;

import java.io.IOException;

public class KafkaProfileMatchInfoQueue extends AbstractKafkaQueue<ProfileMatchInfo> implements ProfileMatchInfoQueue {

    public KafkaProfileMatchInfoQueue(KafkaSettings settings) {
        super(settings, "profile_match_infos");
    }

    @Override
    protected String getKey(ProfileMatchInfo msg) {
        return msg.id;
    }

    @Override
    protected byte[] serialize(ProfileMatchInfo msg) throws IOException {
        return new byte[]{};
    }

}
