package com.example.kafka;

import java.util.Set;

public interface Validatable {

    /**
     * Validates any errors on an entity given that entities business rules or simple null field checks.
     *
     * @return Errors, if any are applicable to the specified entity.
     */
    Set<String> validate();

}
