package com.example.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import org.I0Itec.zkclient.exception.ZkException;

public abstract class AbstractKafkaQueue<T> implements Closeable, Queue<T> {

    private static final Logger log = LoggerFactory.getLogger(AbstractKafkaQueue.class);
    private final String TOPIC;

    private final KafkaClient client;

    public AbstractKafkaQueue(KafkaClient client, String topic) {
        this.TOPIC = topic;
        this.client = client;
    }

    public AbstractKafkaQueue(KafkaSettings settings, String topic) {
        this(connect(settings, 0), topic);
    }

    /**
     * Attempt to connect to zookeeper, sometimes when running for first time during puppet initialization
     * zookeeper could not yet be up. We allow (N) number of attempts to connect to zookeeper.
     * @param settings
     * @param attempts
     */
    private static KafkaClient connect(KafkaSettings settings, int attempts) {
        if (attempts >= settings.maxRetry) {
            log.error("Unable to connect to zookeeper after " + attempts + " # of retries");
            throw new RuntimeException("Unable to connect to zookeeper after retrying " + attempts);
        }

        try {
            KafkaClientOptions options = new KafkaClientOptions(settings.zookeeper, settings.zookeeper);
            options.batch_num_messages = settings.batchSize;
            options.groupId = settings.applicationId;
            options.zookeeperTimeout = settings.zookeeperTimeout;
            options.async = true;
            return new KafkaClient(options);
        } catch (ZkException e) {
            attempts += 1;
            log.warn("Unable to connect to zookeeper @ " + settings.zookeeper + ", retrying...");
            return connect(settings, attempts);
        }
    }

    /**
     * <p>
     * Returns the key that is used to partition the message by kafka to get ordering
     * guaranteed and delivery semantics.
     * </p>
     *
     * <p>For example, extracting a key from a user
     * based message such as 'userID' will allow for any single client to always get
     * messages for that client, correlating the message and ensuring two different
     * clients don't get partial messages.</p.
     *
     * @param msg The message to extract the key for.
     *
     * @return The key used to partiction the messages.
     */
    protected abstract String getKey(T msg);

    /**
     * Serializes the messages into a form for kafka to use and clients to read. For example
     * serializing the object from AVRO to bytes or to JSON to bytes. Do not both compressing
     * as Kafka will manage compression of the messages.
     *
     * @param msg The message to serialize
     * @return The bytes of the serialized object.
     * @throws IOException
     */
    protected abstract byte[] serialize(T msg) throws IOException;

    /**
     * Returns the partition key used for the message when sending.
     *
     * @param msg The message being sent.
     * @return The logical partition key used for sending this message
     */
    public String getPartitionKey(T msg) {
        String key = getKey(msg);
        return Sample.hash(key, 3);
    }

    /**
     * Publish a collection of messages to kafka. Note that this method is async and will no block
     * your thread.
     *
     * @param messages The messages to publish.
     */
    public void publish(final List<T> messages) {
        if (messages == null) return;

        for (T msg : messages) {
            publish(msg);
        }
    }

    /**
     * Publish a single message to kafka. Note that this method is async and will no block
     * your thread.
     *
     * @param msg The message to public.
     */
    public void publish(final T msg) {
        if (msg == null) return;
        try {
            this.publish(getPartitionKey(msg), serialize(msg));
        } catch (Exception ex) {
            log.error("Unable to serialize " + msg.getClass() + " to byte array for kafka publication", ex);
        }
    }

    /**
     * Publish a raw message using the kafka client.
     *
     * @param key The key to write
     * @param msg The message to write.
     */
    protected void publish(String key, byte[] msg) {
        try {
            client.publish(TOPIC, key, msg);
        } catch (Exception ex) {
            log.error("Unable to serialize " + msg.getClass() + " to byte array for kafka publication", ex);
        }
    }

    /**
     * Close and open connections to kafka and ensure flush of events.
     */
    @Override
    public void close() throws IOException {
        // do something
    }

    public KafkaClient getClient() {
        return client;
    }
}
