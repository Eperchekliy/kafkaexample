package com.example.kafka;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.listener.config.ContainerProperties;
import org.jdeferred.Promise;
import org.jdeferred.impl.DeferredObject;

import java.io.Closeable;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class KafkaClient implements Closeable {
    private static final Log log = LogFactory.getLog(KafkaClient.class);

    private Map<String, Object> props;
    private KafkaTemplate<String, byte[]> producer;
    private ConcurrentHashMap<String, KafkaMessageListenerContainer<String, byte[]>> containers = new ConcurrentHashMap<>();

    public KafkaClient(KafkaClientOptions kafkaClientOptions) {
        this.props = (Map) kafkaClientOptions.build();
        producer = createTemplate();
    }

    public Promise<String, String, ByteBuffer> listen(String ...topics) {
        final DeferredObject<String, String, ByteBuffer> dfd = new DeferredObject<>();

        ContainerProperties containerProps = new ContainerProperties(topics);
        containerProps.setListenerTaskExecutor(new SimpleAsyncTaskExecutor());
        containerProps.setMessageListener((MessageListener<String, byte[]>) message -> {
            try {
                ByteBuffer payload = ByteBuffer.wrap(message.value());
                dfd.notify(payload);
            } catch (Exception ex) {
                log.error("Unable to process kafka message, see inner exception for details", ex);
                throw new RuntimeException("Unable to process messages in kafka, " + ex.getMessage());
            }
            dfd.resolve("done");
        });

        KafkaMessageListenerContainer<String, byte[]> container = createContainer(containerProps);
        for (String topic : topics) {
            containers.put(topic, container);
        }

        container.start();

        return dfd.promise();
    }

    private KafkaMessageListenerContainer<String, byte[]> createContainer(ContainerProperties containerProps) {
        DefaultKafkaConsumerFactory<String, byte[]> cf = new DefaultKafkaConsumerFactory<>(props);
        return new KafkaMessageListenerContainer<>(cf, containerProps);
    }


    public void publish(String topic, String key, byte[] message) {
        if (topic == null) {
            throw new RuntimeException("Topic passed to Kafka publish was null!");
        }
        if (message == null) {
            throw new RuntimeException("Byte[] array passed to Kafka publish was null!");
        }
        if (key == null) {
            throw new RuntimeException("key passed to Kafka publish was null!");
        }

        producer.send(topic, key, message);
        producer.flush();
    }

    private KafkaTemplate<String, byte[]> createTemplate() {
        ProducerFactory<String, byte[]> pf = new DefaultKafkaProducerFactory<>(props);
        return new KafkaTemplate<>(pf);
    }

    @Override
    public void close() throws IOException {
        for (KafkaMessageListenerContainer<String, byte[]> kafkaMessageListenerContainer : containers.values()) {
            kafkaMessageListenerContainer.stop();
        }
    }
}
