package com.example.kafka;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Information describing match associations between two or more profiles.
 *
 */
@XmlRootElement
public class ProfileMatchInfo implements Validatable {

    /**
     * Unique id of the profile match info itself (not a profile id).
     */
    public String id = UUID.randomUUID().toString();

    /**
     * The network id associated with the match.
     */
    public String network_id;

    /**
     * The system id for the system that provided the matches. It lets Catalina differentiate between matches that are provided by different systems,
     * that involve the same ids. For example, Target provides matches to indicate Red Card replacement, and also matches to define a 'consumer'.
     * Another example would be a household match from Meijer, and a household match from Experian that use the same loyalty cards. In a cookie
     * match, system id would identify the system that set the cookie.
     * Since Internet cookie matches often use integers (as opposed to UUIDs), this is a Catalina managed identifier.
     */
    public String system_id;

    /**
     * The date the matches begin to be considered valid (ex: "2016-10-01T00:00:00Z").
     */
    public String effective_date;

    /**
     * The date the matches are no longer considered valid (ex: "2017-10-01T00:00:00Z").
     */
    public String expiration_date;

    /**
     * The list of ids, in DMP canonical format, that are related to each other by this match association.
     */
    public List<String> matches;

    /**
     * required for json serializaton
     */
    public ProfileMatchInfo() {
        // Do nothing
    }

    /**
     * Basic validation of the profile match info.
     *
     * @return A set of error strings
     */
    @Override
    public Set<String> validate() {
        Set<String> errors = Sets.newHashSet();

        if (matches == null) {
            errors.add("MISSING_MATCHES");
        } else if (matches.isEmpty()) {
            errors.add("MATCHES_EMPTY");
        } else if (matches.size() < 2) {
            errors.add("MATCHES_NEEDS_TWO_ELEMENTS");
        } else {
            Set<String> temp_matches_set = ImmutableSet
                    .copyOf(Iterables.filter(matches, Predicates.notNull()));
            if (matches.size() != temp_matches_set.size()) {
                errors.add("MATCHES_MUST_BE_A_SET");
            }
        }

        return errors;
    }
}
