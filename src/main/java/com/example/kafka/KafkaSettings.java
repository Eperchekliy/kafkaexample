package com.example.kafka;

public class KafkaSettings {

    /**
     * Creates a new {@link KafkaSettings} using spring
     * dependency injection.
     *
     * @param zookeeper
     *            The zookeeper instance to connect to
     * @param compression
     *            The compression type to use
     * @param maxRetry
     *            The max retry period
     * @param batchSize
     *            The batch size to use when sending messages
     * @param applicationId
     *            The custom application identifier to use
     */
    public KafkaSettings(String zookeeper, Compression compression, int maxRetry, int batchSize, String applicationId) {
        this.zookeeper = zookeeper;
        this.compression = compression;
        this.maxRetry = maxRetry;
        this.batchSize = batchSize;
        this.applicationId = applicationId;
    }

    /**
     * Default constructor for this {@link KafkaSettings}.
     */
    public KafkaSettings() {

    }

    /**
     * The zookeeper instance to connect to for publishing kafka messages.
     */
    public String zookeeper;

    /**
     * The compression to use when sending messages, by default 'NONE'.
     */
    public Compression compression = Compression.NONE;
    /**
     * The maximum retry times for pushing messages.
     */
    public int maxRetry = 3;
    /**
     * The batch size for messages pushed to kafka.
     */
    public int batchSize = 200;

    /**
     * Default timeout when trying to reach zookeeper.
     */
    public int zookeeperTimeout = 600000;

    /**
     * The unique identifier for the application publishing the messages.
     */
    public String applicationId = "";

    /**
     * The type of compression supported by Kafka.
     *
     * @author Terrance A. Snyder
     *
     */
    public static enum Compression {
        /**
         * No Compression
         */
        NONE(0),
        /**
         * Gzip compression (optimized for compression vs. speed)
         */
        GZIP(1),
        /**
         * Snappy compression (optimized for speed vs. compression)
         */
        SNAPPY(2);

        private final int value;

        private Compression(int value) {
            this.value = value;
        }

        /**
         * Gets the value of the enum mapped to Kafka's switch codes for
         * compression codecs.
         *
         * @return
         */
        public int getValue() {
            return value;
        }
    }
}
