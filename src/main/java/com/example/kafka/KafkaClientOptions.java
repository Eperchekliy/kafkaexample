package com.example.kafka;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Properties;

public class KafkaClientOptions {
    /**
     * Initialize a kafka client with performant defaults against the specified zookeeper instance.
     *
     * @param zookeeper
     */
    public KafkaClientOptions(String zookeeper, String brokers) {
        this.zookeeper = zookeeper;
        this.brokers = Lists.newArrayList(brokers.split(","));
    }

    private List<String> brokers;

    /**
     * The zookeeper instance to connect to.
     */
    public String zookeeper;

    /**
     * The default timeout in milliseconds when connecting to zookeeper (by default 60 seconds)
     */
    public Integer zookeeperTimeout = 60000;

    /**
     * The ID by which we want to consume messages, used for delivery of messages to a federated
     * cluster of subscribers.
     */
    public String groupId = "";

    /**
     * The default number of threads to consume messages for, by default 1.
     */
    public Integer numberThreads = 1;

    /**
     * The maximum message size (set to 1GB by default)
     */
    public Integer maxMessageSize = 1073741824;

    /**
     * The maximum size to fetch messages from.
     */
    public Integer maxFetchMessageSize = 1073741824;

    /**
     * The period to commit kafka offsets back to zookeeper.
     */
    public Integer commit_offset_ms = 60 * 1000;

    /**
     * The default number of messages to batch, by default 1000.
     */
    public Integer batch_num_messages = 1000;

    /**
     * If the messages you want to write are async (reduced ability for durable messages).
     */
    public Boolean async = false;

    public Properties build() {
        Properties props = new Properties();
        props.put("zk.connect", zookeeper);
        props.put("zookeeper.connect", zookeeper);
        props.put("metadata.broker.list", Joiner.on(",").join(brokers));
        props.put("bootstrap.servers", Joiner.on(",").join(brokers));

        props.put("zk.connectiontimeout.ms", String.valueOf(zookeeperTimeout));
        props.put("zookeeper.connection.timeout.ms", String.valueOf(zookeeperTimeout));
        props.put("groupid", groupId);
        props.put("group.id", groupId);
        props.put("producer.type", async ? "async" : "sync");
        props.put("auto.commit.interval.ms", String.valueOf(commit_offset_ms));
        props.put("mirror.consumer.numthreads", String.valueOf(numberThreads));
        props.put("fetch.message.max.bytes", String.valueOf(maxFetchMessageSize));
        props.put("message.max.bytes", String.valueOf(maxMessageSize));
        props.put("max.message.size", String.valueOf(maxMessageSize));
        props.put("producer.type", "async"); // (async or sync)
        props.put("replica.fetch.max.bytes", String.valueOf(maxMessageSize));
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer");

        props.put("key.deserializer","org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer","org.apache.kafka.common.serialization.ByteArrayDeserializer");

        // 0 - which means that the producer never waits
        // 1 - which means that the producer gets an acknowledgement after the leader replica has received the data
        // -1, which means that the producer gets an acknowledgement after all in-sync replicas have received the data
        props.put("request.required.acks", "0");

        props.put("replica.fetch.max.bytes", String.valueOf(maxMessageSize));
        props.put("batch.size", String.valueOf(batch_num_messages));
        props.put("batch.num.messages", String.valueOf(batch_num_messages));

        // breaking change 2 == SNAPPY or use string?
        props.put("compression.codec", "2");

        return props;
    }
}
