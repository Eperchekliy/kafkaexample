package com.example.kafka;

public interface ProfileMatchInfoQueue {
    public void publish(final ProfileMatchInfo profile_match_info);
}
